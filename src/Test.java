public class Test {

    public static void main(String[] args){
        Automate automate = new Automate();

        System.out.println(automate.getGift());
        automate.putMoney();
        System.out.println(automate.getGift());

        MarioStrategy marioStrategy = new MarioStrategy();
        Context context = new Context(marioStrategy);

        System.out.println(context.doAction(Actions.LEFT_ACTION));
    }
}
