public class ActiveState extends State {

    private static final String GIFT_STRING = "Take your gift";

    public ActiveState(Automate automate) {
        super(automate);
    }

    @Override
    void putMoney() {
    }

    @Override
    String getGift() {
        return GIFT_STRING;
    }
}
