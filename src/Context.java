public class Context {
    private Strategy mStrategy;

    public Context(Strategy strategy){
        mStrategy = strategy;
    }

    public String doAction(Actions action){
        return mStrategy.doAction(action);
    }
}
