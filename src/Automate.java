public class Automate {

    private State mState;

    public Automate(){
        mState = new InactiveState(this);
    }

    public void changeState(State state){
        mState = state;
    }

    public boolean isStateActive(){
        return mState instanceof ActiveState;
    }

    public void putMoney(){
        mState.putMoney();
    }

    public String getGift(){
        return mState.getGift();
    }
}
