public interface Strategy {
    String doAction(Actions action);
}
