public abstract class State {

    protected Automate mAutomate;

    public State(Automate automate){
        mAutomate = automate;
    }

    abstract void putMoney();
    abstract String getGift();
}
