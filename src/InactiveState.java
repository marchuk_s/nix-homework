public class InactiveState extends State {

    private static final String ERROR_STRING = "Error!";

    public InactiveState(Automate automate) {
        super(automate);
    }

    @Override
    void putMoney() {
        if (!mAutomate.isStateActive()){
            mAutomate.changeState(new ActiveState(mAutomate));
        }
    }

    @Override
    String getGift() {
        return ERROR_STRING;
    }
}
