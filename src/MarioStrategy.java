import java.util.HashMap;

public class MarioStrategy implements Strategy {
    private static final String MARIO_UP_ACTION = "Mario jumps";
    private static final String MARIO_DOWN_ACTION = "Mario sits";
    private static final String MARIO_LEFT_ACTION = "Mario moves left";
    private static final String MARIO_RIGHT_ACTION = "Mario moves right";
    private static final String MARIO_FIRE_ACTION = "Mario shoots";

    private HashMap<Actions, String> mActionMap = new HashMap<>(){{
        put(Actions.UP_ACTION, MARIO_UP_ACTION);
        put(Actions.DOWN_ACTION, MARIO_DOWN_ACTION);
        put(Actions.LEFT_ACTION, MARIO_LEFT_ACTION);
        put(Actions.RIGHT_ACTION, MARIO_RIGHT_ACTION);
        put(Actions.FIRE_ACTION, MARIO_FIRE_ACTION);
    }};

    @Override
    public String doAction(Actions action) {
        return mActionMap.get(action);
    }
}
